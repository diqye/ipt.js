- 大坑小坑都是坑坑
- 支持依赖项
- 支持各种路径绝对相对路径
- 循环依赖
- 多模块合并

## 开始
```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ipt-test</title>
</head>
<body>
	<script src="../js/ipt.js"></script>
	<script>
      ipt('./c.js')('./c.css');
      //同一个js只会加载一次
      ipt('./c.js')('./c.js')('./c.js').wait(function(err,c1,css,c2,c3,c4){
         var div=document.createElement('div');
          div.innerHTML="c.js，c.css加载完毕，我是index.html <br/>"+[c1,c2,c3].join('<br/>')
          document.body.appendChild(div);
      });
	  iife(function(exports,modules){
		  //向外部导出的三中方式
		  exports.a='exp1';
		  this.b='exp2';
		  modules.exports.c='exp3';
	  });
	  
	 
	</script>
</body>
</html>
```
### c.js
```javascript
//'/' 为页面中的iife
iife(['/'],function(exp,m,home){
	m.exports=home.a+home.b+home.c;
})
//也可以使用define 
/*
define(['./a.js],function(a){
 //向外导出
 return xxx;
})
*/
```
### 结果

c.js，c.css加载完毕，我是index.html 

exp1exp2exp3

exp1exp2exp3

exp1exp2exp3

### 引入jquery(jquery的模块id是在JQuery内部定死的所以必须配置)
```javascript
ipt({
	jquery:'http://cdn.bootcss.com/jquery/3.0.0-alpha1/jquery.js'
})
('./test1.js?v=1')
('jquery')
.wait(function(err,test,jquery1){
	logln('test is '+test);
	logln('jquery is '+jquery1);
})

```

### wait函数

```html
<script>
	/*
	*并行加载a.js,b.js,c.js当三个js加载完成 加载c.js
	*wait()('./c.js') 等同于 wait().ipt('./c.js')
	*/
	ipt('./a.js')('./b.js')('./c.js').wait()('./c.js');
	/*
	*以下代码等同于 上面的代码
	*/
	ipt('./a.js')('./b.js')('./c.js');
	ipt.wait()('./c.js');
	
</script>
```