/*
	util js工具函数
*/
var util = (function (exports) {

    //遍历 并列执行 结果反馈给回调
    function map(arr, fn, call) {
            if (!arr || arr.length == 0) call();
            if (Object.prototype.toString.call(arr) === '[object Array]') {
                var rs = [],
                    rerr = null,
                    num = 1;
                for (var i = 0, l = arr.length; i < l; i++) {
                    ! function (t, i) {
                        fn(function (err, res) {
                            if (err) rerr = err;
                            else rs[i] = res;
                            if (num++ == l) call(rerr, rs);
                        }, t, i);
                    }(arr[i], i)
                }
            } else if (Object.prototype.toString.call(arr) === '[object Object]') {
                var keys = [],
                    num = 1,
                    rs = {},
                    rerr = null;
                for (var key in arr) keys.push(key);
                for (var key in arr) {
                    ! function (t, key) {
                        fn(function (err, res) {
                            if (err) rerr = err;
                            else rs[key] = res;
                            if (num++ == keys.length) call(rerr, rs);
                        }, t, key);
                    }(arr[key], key)
                }
            }
        }
        //遍历 序列执行 结果分级向下反馈
    function mapsear(arr, fn, call) {
            if (!arr || arr.length == 0) call();
            if (Object.prototype.toString.call(arr) === '[object Array]') {
                var rs = [],
                    rerr = null,
                    len = arr.length;
                ! function run(i) {
                    if (i == len) {
                        call(null, rs);
                        return;
                    }
                    var a = arr[i];
                    fn(function (err, para) {
                        if (err) {
                            call(err);
                            return;
                        }
                        rs[i] = para;
                        run(i + 1);
                    }, a, i);
                }(0);

            } else if (Object.prototype.toString.call(arr) === '[object Object]') {
                var keys = [],
                    rs = {};
                for (var key in arr) keys.push(key);
                var len = keys.length;
                ! function run(i) {
                    if (i == len) {
                        call(null, rs);
                        return;
                    }
                    var a = arr[keys[i]];
                    fn(function (err, para) {
                        if (err) {
                            call(err);
                            return;
                        }
                        rs[keys[i]] = para;
                        run(i + 1);
                    }, a, keys[i]);
                }(0);
            }
        }
        //数据类型判断
    var type = (function () {

        var r = {},
            types = ['Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp', 'Error', 'Null', 'Array','Object'];
        for (var i = 0, t; t = types[i++];) {
            ! function (t) {
                r['is' + t] = function (obj) {
                    return Object.prototype.toString.call(obj) === '[object ' + t + ']';
                }
            }(t)
        }
        return r;
    })();

    function each(arr, fn) {
        if (!arr || !arr.length) return;
        for (var i = 0, l = arr.length, t; i < l; i++) {
            t = arr[i];
            if (fn(t, i) === false) break;
        }
    }

    function toabspath(src) {
            var a = document.createElement('a');
            a.setAttribute('href', src);
            return a.href; //使用a.getAttribute('href')会不灵的
        }
        //修复document.head
    var dochead = document.head = document.head || document.getElementsByTagName('head')[0];

    function getCurrentScript() {
        if (document.currentScript) return document.currentScript;
        var r = null;
        each(dochead.getElementsByTagName('script'), function (t, i) {
            if (t.readyState === 'interactive') {
                r = t;
            }
        })
        if (r) return r;
        try {
            "呵呵".恶心的浏览器只能以恶心的方式去处理();
        } catch (e) {
            var stack = e.stack;
            if (!stack) {
                stack = (String(e).match(/if linked scipt \s+/g) || []).join(' ');
            }
            stack = stack.split(/[@ ]/g).pop();
            stack = stack[0] === '(' ? stack.slice(1, -1) : stack.replace(/\s/, '');
            return {
                src: stack.replace(/(:\d+)?:\d+$/i, "")
            };
        }
    }
	
	function isEmpty(obj){
		for(var key in obj)return false;
		return true;
	}
    return {
        dochead: dochead,
        each: each,
        map: map,
        mapsear: mapsear,
        type: type,
		isEmpty:isEmpty,
        getCurrentScript: getCurrentScript,
        toabspath: toabspath
    }
}());