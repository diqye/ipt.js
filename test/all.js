function append(s){
	var div=document.createElement('div');
		div.innerHTML=s;
		document.body.appendChild(div);
}
//文件内 多个iife必须指定id
iife('./1.js',['./3.js'],function(e,m,m3){
	append('1.js--'+m3)
})
iife('./3.js',['./2.js'],function(e,m){
	append('3.js')
	m.exports='3.js'
})
iife('./2.js',['./1.js'],function(e,m,m1){
	append('2.js')
})